import express from 'express'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import routes from './routes.js'

export default function createServer() {
  const app = express()
  app.use(express.json())
  app.use(cors())
  app.use(cookieParser())
  routes(app);
  return app
}

import productModel from "../models/product.model";
import { Request, Response } from "express";

export async function createProduct(req: Request, res: Response){
    const product = new productModel(req.body);
    try {
        const newProduct = await product.save();
        res.status(201).json(newProduct);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function getProducts(req: Request, res: Response) {
    try {
        const products = await productModel.find();
        res.status(200).json(products);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function getProduct(req: Request, res: Response) {
    try {
        const product = await productModel.findById(req.params.id);
        res.status(200).json(product);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function updateProduct(req: Request, res: Response){
    try {
        const product = await productModel.findByIdAndUpdate(
            req.params.id,
            {
                $set: req.body
            },
            {
                new: true
            }
        );
        res.status(200).json(product);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function deleteProduct(req: Request, res: Response){
    try {
        await productModel.findByIdAndDelete(req.params.id);
        res.status(200).json("Product has been deleted...");
    } catch (error) {
        res.status(500).json(error);
    }
}

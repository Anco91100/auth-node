import cartModel from "../models/cart.model";
import { Request, Response } from "express";

export async function createCart(req: Request, res: Response) {
    const cart = new cartModel(req.body);
    try {
        const newCart = await cart.save();
        res.status(201).json(newCart);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function getCart(req: Request, res: Response) {
    try {
        const cart = await cartModel.find({userId: req.params.userId});
        res.status(201).json(cart);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function getCarts(req: Request, res: Response) {
    try {
        const carts = await cartModel.find();
        res.status(201).json(carts);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function updateCart(req: Request, res: Response){
    try {
        const cart = await cartModel.findByIdAndUpdate(
            req.params.id,
            {
                $set: req.body
            },
            {
                new: true
            }
        );
        res.status(201).json(cart);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function deleteCart(req: Request, res: Response) {
    try {
        await cartModel.findByIdAndDelete(req.params.id);
        res.status(201).json('Cart has been deleted ...');
    } catch (error) {
        res.status(500).json(error);
    }
}


import UserModel from '../models/user.model'
import jwt from 'jsonwebtoken'
import { Request, Response } from 'express'
import bcrypt from 'bcryptjs'
const maxAge = 3 * 24 * 60 * 60 * 1000

export async function register(req: Request, res: Response) {
  try {
    const { email, password, pseudo} = req.body
    if (!email || !password) {
      res.status(400).json({ message: 'Error while authenticating', isAuthValid: !email || !password })
    }
    const isUserExists = await UserModel.findOne({ email: email })
    if (isUserExists) {
      res.json({ message: 'The user already exists', isUserExists })
    }
    const salt = await bcrypt.genSalt()
    const hashPassword = await bcrypt.hash(password, salt)
    const newUser = new UserModel({
      pseudo,
      email,
      password: hashPassword,
    })
    const savedUser = await newUser.save()
    res.status(201).json(savedUser)
  } catch (error) {
    res.status(500).json({error: error});
  }
}

export async function login(req: Request, res: Response) {
  try {
    const { email, password } = req.body

    const isUserExists = await UserModel.findOne({ email: email })
    if (!isUserExists) return res.status(400).json({ msg: 'User does not exist. ', isUserExists })
    const isPasswordMatch = await bcrypt.compare(password, isUserExists.password)
    if (!isPasswordMatch) return res.status(400).json({ msg: ' Invalid password. ', isPasswordMatch })

    const token = jwt.sign({ user: isUserExists.email }, 'secret', { expiresIn: maxAge })
    res.cookie('jwt', token, { httpOnly: true, maxAge })
    return res.status(200).json({ token, isUserExists })
  } catch (error) {
    return res.status(400).json({ error: error })
  }
}

export async function logout(req: Request, res: Response) {
  res.cookie('jwt', '', { maxAge: 1 })
  res.send({ msg: 'Disconnected' })
}

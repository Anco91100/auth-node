import orderModel from '../models/order.model';
import { Request, Response } from 'express';

export async function createOrder(req: Request, res: Response) {
    const order = new orderModel(req.body);
    try {
        const newOrder = await order.save();
        res.status(201).json(newOrder);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function getOrders(req: Request, res: Response) {
    try {
        const orders = await orderModel.find();
        res.status(201).json(orders);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function getOrder(req: Request, res: Response) {
    try {
        const order = await orderModel.find({userId: req.params.userId});
        res.status(201).json(order);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function updateOrder(req: Request, res: Response) {
    try {
        const order = await orderModel.findByIdAndUpdate(
            req.params.id,
            {
                $set: req.body
            },
            {
                new: true
            }
        );
        res.status(201).json(order);
    } catch (error) {
        res.status(500).json(error);
    }
}

export async function deleteOrder(req: Request, res: Response) {
    try {
        await orderModel.findByIdAndDelete(req.params.id);
        res.status(201).json('Order has been deleted ...');
    } catch (error) {
        res.status(500).json(error);
    }
}
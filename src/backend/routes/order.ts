import { Router } from 'express';
import { createOrder, deleteOrder, getOrder, getOrders, updateOrder } from '../controllers/order.controller';

const router = Router();

router.post('/create', createOrder);
router.get('/orders', getOrders);
router.get('/find/:userId', getOrder);
router.put('/update/:id', updateOrder);
router.delete('/delete/:id', deleteOrder);

export default router;

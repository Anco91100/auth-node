import { Router } from "express";
import {createProduct, updateProduct, getProduct, getProducts, deleteProduct } from '../controllers/product.controller';

const router = Router();

router.post('/create', createProduct);
router.put('/update/:id', updateProduct);
router.get('/', getProducts);
router.get('/find/:id',getProduct);
router.delete('delete/:id', deleteProduct);

export default router;
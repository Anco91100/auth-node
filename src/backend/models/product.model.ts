import mongoose from "mongoose";

const productSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true
    },
    desc: {
        type: Number,
        required: true
    },
    img: {
        type: String
    },
    color: {
        type: String
    }
},
{ timestamps: true }
);

const productModel = mongoose.model("product",productSchema);
export default productModel;
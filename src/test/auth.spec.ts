import supertest from 'supertest'
import createServer from '../server.js';
import { MongoMemoryServer } from "mongodb-memory-server";
import mongoose from "mongoose";
import { Application, Express } from 'express';
import {jest} from '@jest/globals';

const app = createServer();
jest.setTimeout(10000);
interface User {
    email: string,
    password: string,
    bio: string,
    pseudo: string,
    followers: string[],
    following: string[],
    picture: string
}
const userId = new mongoose.Types.ObjectId();
// it('Gets the test endpoint', async () => {
//   // Sends GET Request to /test endpoint
//   const res = await request.get('/test')
//   expect(res.status).toBe(200)
//   expect(res.body.message).toBe('pass!')
// })

describe('Register test', () => {
    beforeAll(async () => {
        const mongoServer = await MongoMemoryServer.create();
    
        await mongoose.connect(mongoServer.getUri());
      });
    
      afterAll(async () => {
        await mongoose.disconnect();
        await mongoose.connection.close();
      });
    const mockUser: Pick<User, "email" | "bio" | "pseudo" | "password"> = {
        "email": "test@test.fr",
        "password": "testsecret123",
        "bio": "ceci est un test",
        "pseudo": "mocktest"
    }
    const wrongUser: Pick<User, "email" | "bio" | "pseudo"> = {
        "email": "wrong@test.fr",
        "bio": "wrong user",
        "pseudo": "wrong user"
    }
    it('Should register a new user', async()=> {
        const {statusCode} = await supertest(app).post('/auth/register').send({
            "email": "test@test.fr",
            "password": "testsecret123",
            "bio": "ceci est un test",
            "pseudo": "mocktest",
            "followers": [],
            "following": [],
            "picture": ""
        })
        expect(statusCode).toBe(201);
    });
    // it('Should not register a new user', async () => {
    //     const {body, statusCode} = await request.post('/auth/register').send(wrongUser);
    //     expect(statusCode).toBe(400);
    //     console.log(body, statusCode)
    // });
})
#!/usr/bin/env node

import 'dotenv/config'
import { Config } from './config.js'
import './connect.js';
import createServer from './server.js';
const app = createServer();

app.listen(Config.port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server started on port: ${Config.port}`)
});

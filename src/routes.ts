import { Express } from 'express'
import authAPI from './backend/routes/auth.js'
import orderApi from './backend/routes/order.js';
import productApi from './backend/routes/product.js'

export default function routes(app: Express) {
  app.use('/auth', authAPI)
  app.use('/order', orderApi);
  app.use('/product', productApi);
  app.get('/test', async (req, res) => {
    res.json({ message: 'pass!' })
  })
}
